package com.utility;

import java.util.List;

public interface I_ReadEnvironments {

	public List getAllEnvironments(String path);
	public void getCurrentEnvironmentConfigFile(String jobName);
}
