package com.utility;

import java.util.HashMap;

public interface I_ReadJsonFile {
 public HashMap readJson(String path);
 public String readText(String path);
}
