package com.utility


import groovy.json.JsonSlurper;
class ReadFile implements I_ReadJsonFile{

	@Override
	public HashMap readJson(String path) {
		def inputFile = new File(path)
		HashMap InputJSON = new JsonSlurper().parseText(inputFile.text)
		return InputJSON;
	}

	@Override
	public String readText(String path) {
		return new File(path).getText('UTF-8')
	}
}
